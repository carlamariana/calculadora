module ExtraOperations
  def fibonacci(number)
    sequence = [] 
    f1 = 0
    f2 = 1
    number.times do |numero|
      aux = f1
      f1 = f2
      f2 = aux + f2
      sequence.push(f1)
    end
    return sequence
  end
end

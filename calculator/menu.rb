require_relative 'operations'

module Calculator
  class Menu
    def initialize
      operations = Operations.new
      puts '################################'
      puts '  Seja bem vindo à calculadora'
      puts '################################'
      puts 'Selecione uma das opções a seguir:'
      puts '|1| Calcule a média'
      puts '|2| Saiba os números divisíveis por 25'
      puts '|3| Filtre os filmes'
      puts '|4| Conheça a sequência de Fibonacci'
      puts '|0| para sair'
      print 'Sua opção: '
      option = gets.chomp.to_i
      
      case option
      when 1
        print 'Digite o nome dos alunos e suas respectivas notas: '
        grades = gets.chomp
        print 'Digite o nome dos alunos que deseja exluir da média: '
        blacklist = gets.chomp
        result = operations.biased_mean(grades, blacklist)
        puts "A média dos alunos é #{result}"
      when 2
        print 'Digite a sequência de números: '
        numbers = gets.chomp
        result = operations.no_integers(numbers)
        puts "#{result}"
      when 3
        print 'Digite os gêneros dos filmes: '
        genres = gets.chomp
        print 'Digite a partir de que ano você quer: '
        year = gets.chomp
        result = operations.filter_films(genres, year)
        puts result
        if (result == [])
          puts 'Não há nenhum filme com essas características'
        end
      when 4
        print 'Digite o número(n) para que você saiba a sequência de Fibonacci até a énesima posição: '
        number = gets.chomp.to_i
        result = operations.fibonacci(number)
        puts result
      when 0
        exit
      else
        puts 'Operação inválida'
      end
    end
  end
end

require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      notes = JSON.parse(grades)
      names = blacklist.split(" ")
      soma = 0.0
      notes.each do |key, value|
        soma += value.to_f
      end
      notes.each do |key, value|
        names.each do |name|
          if name == key
            soma -= value.to_f
          end
        end
      end
      media = soma/(notes.size - names.size)
      return media
    end
  
    def no_integers(numbers)
      resultado = ""
      numbers = numbers.split(" ")
      numbers.each do |number|
          algarisms = number.to_i - ((number.to_i/100).to_i)*100
          if (algarisms == 0 || algarisms == 25 || algarisms == 50 || algarisms == 75)
            resultado += "S "
          elsif (number == 0 || number == 25 || number == 50 || number == 75)
            resultado += "S "
          else
            resultado += "N "
          end
      end
      return resultado
    end
  
    def filter_films(genres, year)
      film = []
      genres = genres.split(" ")
      films = get_films[:movies]
      films.each do |characteristic|
        if (characteristic[:year] >= year && (genres & characteristic[:genres]) == genres)
          film.push(characteristic[:title])
        end
      end
      return film
    end
  
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
